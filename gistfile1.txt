https://www.curseforge.com/wow/addons/wowsimsexporter
For search in curseforge app: wowSims Exporter 

Ingame type /wse
Export spec / gear

Download sim:
https://github.com/wowsims/wotlk/releases ---> https://github.com/wowsims/wotlk/releases/download/v0.1.41/wowsimwotlk-windows.exe.zip

Extract, copy exe to desktop or wherever you want it.
Click it, "more info", "Run anyway"

local sim much quicker than online version (5x maybe?) 
Online version as alternative if you don't want to download the exe: https://wowsims.github.io/wotlk/

Open the sim for the class you play.
Import -> Addon -> paste the export wall of text from the addon

Change iterations to 10000 or 20000
Check Settings -> Buffs -> Misc for Draenaei etc.

Simulate gear, check what is better etc.
"Save Reference" "Save gearset" is useful as well .... 
